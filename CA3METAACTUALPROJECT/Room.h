#ifndef ROOM_H
#define ROOM_H
//This is the room class. 
#include <vector>
#include "Enemy.h"
//forward declaration of player class
class Player;
class Room
{
private:
	std::string m_name;
	std::string m_desc;
	std::vector<Room*> m_map;
	ItemStore m_items;
	bool m_locked;
	bool m_discovered;
	Enemy* m_enemy;
public:
	Room();
	Room(const std::string &p_name, const std::string &p_desc);
	std::string getName();
	void setName(const std::string &p_name);
	std::string getDescription();
	void setDescription(const std::string &p_desc);
	
	void unLock();
	void lock();
	bool isLocked();

	void addNearRooms(Room* n, Room* s, Room* e, Room* w, Room* u, Room* d);

	void print();
	
	void printRooms();
	void printRoomsNicely();

	void printItems();
	void printItemsNicely();
	void addItemToStore(Item* p_item);
	void removeItem(Item* p_item);

	void discover();
	bool isDiscovered();

	bool exists();
	bool attemptEntry(Player &p_player);

	Room* getNorthRoom();
	Room* getSouthRoom();
	Room* getEastRoom();
	Room* getWestRoom();
	Room* getUpperRoom();
	Room* getLowerRoom();

	bool hasItem(Item* p_item);

	void setEnemy(Enemy* p_enemy);
	Enemy* getEnemy();
	bool hasEnemy();

	~Room();
};
#endif
