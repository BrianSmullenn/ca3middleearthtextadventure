#ifndef PUZZLES_H
#define PUZZLES_H
//Just some code to check for puzzles and battles and fullfill them
#include "Player.h"
#include "Enemy.h"
bool puzzle1(Player* p_player, ItemStore* p_items);
bool puzzle2(Player* p_player, ItemStore* p_items);
bool battle(Player* p_player, Enemy* p_enemy);
#endif