#ifndef ENEMY_H
#define ENEMY_H
//Enemy class. Creates an enemy that is stored in a room. 
//Similar to player stats. Used for battling in game
#include "Item.h"
class Enemy
{
private:
	std::string m_name;
	std::string m_desc;
	std::string m_defeat;
	std::string m_loss;
	
	int m_lvl;
	int m_hp;
	int m_ob;
	int m_db;
	
	ItemStore m_loot;

	int m_expReward;
public:
	Enemy();
	Enemy(const std::string& p_name, const std::string& p_desc, const std::string& p_defeat, const std::string& p_lose, int p_lvl, int p_hp, int p_ob, int p_db, int p_exp);

	std::string getName();
	std::string getDesc();
	std::string getDefeatMsg();
	std::string getLossMsg();
	
	int getHP();
	int getLV();
	int getOB();
	int getDB();

	int getExpReward();

	void setName(const std::string& p_name);
	void setDesc(const std::string& p_desc);
	void setDefeatMsg(const std::string& p_defeat);
	void setLossMsg(const std::string& p_lose);

	void setHP(int p_hp);
	void setLV(int p_lv);
	void setOB(int p_ob);
	void setDB(int p_db);

	void reduceHP(int p_damage);
};
#endif