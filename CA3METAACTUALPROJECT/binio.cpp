//SHANES CODE!

/**
 * @author  Shane Gavin <shout@nodehead.com> <http://nodehead.com/>
 * @version 0.1
 * @since   0.1
 *
 * This file defines a number of free functions which can be used to perform
 * operations on, and interact with, binary files. These functions are sub-
 * grouped into the ubelt::binio namespace. Given the small amount of
 * functionality within, a using namespace... should be acceptable in most
 * (though not all) circumstances.
 *
 * Copyright (c) Shane Gavin 2014
 *
 * This file is part of the ubelt namespace.
 *
 * the ubelt namespace is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * the ubelt namespace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the ubelt namespace. If not, see <http://www.gnu.org/licenses/>.
 */

//#############################################################################
// Dependencies
//#############################################################################

#include "binio.h"


//#############################################################################
// Namespace resolution
//#############################################################################

using std::string;


//#############################################################################
// The Meat
//#############################################################################

namespace ubelt{ namespace binio
{
    /**
     * Calculate the size (in bytes) of the specified binary file
     * @param  file The file to inspect
     * @return      The file's size, in bytes
     */
    size_t size(FILE * file)
    {
        if (file == 0)
            return 0;

        // Record the file's current pointer position
        size_t startpos = ftell(file);

        // Move to end and inspect
        fseek(file, 0, SEEK_END);
        size_t size = ftell(file);

        // Reset file pointer
        fseek(file, startpos, SEEK_SET);

        return size;
    }

    //#########################################################################
    // String Overloaded forms of templates
    //#########################################################################

    /**
     * Works just like template implementation, but specialized for std::string
     * with the following <b>WARNING:</b>
     *
     * - Passing a string literal is NOT the same as passing a string variable.
     *   Passing a string literal will result in the template being called.
     *
     * @see template <class T> inline bool write()
     */
    bool write(const string& source, FILE * file)
    {
        string::size_type size = source.size();

        // I refuse to write empty strings!
        if (size == 0)
            return false;

        // Write size information
        if (1 != fwrite(&size, sizeof(size), 1, file))
            return false;

        // Write characters
        if (size != fwrite(&source[0], sizeof(char), size, file))
        {
            // Reverse pointer to starting position
            long reverse = size * sizeof(char) * -1L;
            fseek(file, reverse, SEEK_CUR);
            return false;
        }
        else
            return true;
    }

    /**
     * Works just like template implementation, but specialized for std::string
     * with the following <b>WARNING:</b>
     *
     * - Passing a string literal is NOT the same as passing a string variable.
     *   Passing a string literal will result in the template being called.
     *
     * @see template <class T> inline bool read()
     */
    bool read(string& dest, FILE * file)
    {
        string::size_type size;

        // Read size information
        if (1 != fread(&size, sizeof(size), 1, file))
            return false;

        // Size the destination string
        dest.resize(size);

        // Push character to the destination string
        if (size != fread(&dest[0], sizeof(char), size, file))
        {
            long reverse = size * sizeof(char) * -1L;
            fseek(file, reverse, SEEK_CUR);
            return false;
        }
        else
            return true;
    }

    /**
     * Works just like template implementation, but specialized for std::string
     * @see template <class T> inline bool skip()
     */
    template <> bool skip<string>(FILE * file)
    {
        string::size_type size;

        // Read size information
        if (1 != fread(&size, sizeof(size), 1, file))
            return false;
        else
            size *= sizeof(char);

        // 0 indicates a successful seek!
        if (0 != fseek(file, size, SEEK_CUR))
        {
            long reverse = size * sizeof(char) * -1L;
            fseek(file, reverse, SEEK_CUR);
            return false;
        }
        else
            return true;
    }
}}