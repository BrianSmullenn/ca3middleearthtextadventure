#ifndef GAME_H
#define GAME_H

#include <iostream>
#include <vector>
#include "Player.h"

//This is just declartions of some needed game functions. Parsing and game loop for example, aswell
//as get input functions.
std::string getStringInput();
int getInput();
int getInput(int p_min, int p_max);
Room* getRoomByName(const std::string &name, std::vector<Room> &p_rooms);
std::vector<Room*> setUpWorld();
ItemStore addItemsToWorld(std::vector<Room*> p_rooms);
int gameLoop(std::vector<Room*> p_rooms, ItemStore p_items, Player& p_player);
void printro();
void printOutro();
int rollD100();
int getRand();
void printControls();

std::vector<Enemy*> setUpEnemies(std::vector<Room*> rooms);

void cleanUp(ItemStore* p_items, std::vector<Room*>* p_rooms, std::vector<Enemy*>* p_enemies);
#endif