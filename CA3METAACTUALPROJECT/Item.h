#ifndef ITEM_H
#define ITEM_H
//This is the header file for the item class.
#include <string>
#include <list>
class Room;
class Item
{
protected:
	std::string m_name;
	std::string m_desc;
public:
	Item();
	Item(const std::string &p_name, const std::string &p_desc);
	
	std::string getName();
	std::string getDesc();

	bool operator==(Item* p_item);

	virtual void useItem();
	virtual void print();
	virtual void look(Room* p_room);
	virtual void save(std::string& filename);
	virtual void load(std::string& filename);
	virtual void read_object(FILE * in);
	virtual void write_object(FILE * out);
	virtual ~Item();
};


class ItemStore
{
private:
	std::list<Item*> m_list;
	int m_size;
public:
	ItemStore();
	void append(Item* p_item);
	//bool contains(Item& p_item);
	void print();
	void printNicely();
	Item* operator[](int p_index);
	Item* ItemStore::getByName(const std::string& p_name);
	bool ItemStore::contains(Item* p_item);
	int size();
	void remove(Item* p_item);

	void save(std::string& filename);
	void load(std::string& filename);
	void read_object(FILE * in);
	void write_object(FILE * out);
	
	void ItemStore::cleanUp();
	~ItemStore();
};
#endif