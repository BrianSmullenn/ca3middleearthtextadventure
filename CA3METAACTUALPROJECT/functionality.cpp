//////////////////////////////////////////////////////////////////////////////////////////////////////
//Implementation of puzzles and battles///////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
#include "puzzles.h"
#include <iostream>
#include "Game.h"
using std::cout;
using std::endl;
using std::cin;

//////////////////////////////////////////////////////////////////////////////////////////////////////
//Name: Puzzle1///////////////////////////////////////////////////////////////////////////////////////
//Desc: simple puzzle that runs in statue room if player has chalace and ceremonial knife/////////////
//Params: p_player - pointer to player////////////////////////////////////////////////////////////////
//Params: p_items - pointer to itemstore - all items in world/////////////////////////////////////////
//Return: boolean - true if puzzle has happened///////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
bool puzzle1(Player* p_player, ItemStore* p_items)
{
	int input = 0;
	std::string stringInput;

	if(p_player->getCurrentPosition()->getName() == "Statue Room")//In correct room
	{
		if(p_player->hasItem(p_items->getByName("ceremonial knife")))//have knife
		{
			if(p_player->hasItem(p_items->getByName("stone chalice")))//have chalace
			{
				//Then were good to go
				cout << "Some strange is happening. Make a perception roll. Hit \"r\" to roll. Anything else will quit." << endl;
				stringInput = getStringInput();
				if(stringInput == "r")//Roll for puzzle or else just quit
				{	
					int rand = rollD100();
					rand+=p_player->perception();
					if(rand > 50)
					{
						enum choice{YES = 1, NO};
						cout << "A new statue has appeared. Strange, it's a stone vampire with his hand held out toward you." <<
							" Would you like to interact? \n1)Yes\n2)No\n " << endl;
				
						input = getInput();
						switch(input)
						{
						case YES:
							//Some narative parts
							cout << "\nYou place the chalice into the vampires hand and and it drops a bit. " 
								<<	"Using the ceremonial knife you found, you slice your hand and fill "
								<<	"the chalice with blood. As it fills the arm keeps dropping. Nearly full, " 
								<<	"the chalice drops from the vampires hand. Blood spills and in the reflection a "
								<<  "real vampire is standing over you, claws ready to slash. You jolt upright but theres nothing there. "
								<<  "The statue has disapeared. The door north slams shut.\n"<< endl;
							p_player->getCurrentPosition()->getNorthRoom()->unLock();
							p_player->removeItemFromInventory(p_items->getByName("ceremonial knife"));
							p_player->removeItemFromInventory(p_items->getByName("stone chalace"));
							return true;
						}
					}
					else
					{
						cout << "Maybe it's nothing." << endl;
					}
				}
				else
				{
					cout << "Quit puzzle" << endl;
				}
			}
		}
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//Name: Puzzle1///////////////////////////////////////////////////////////////////////////////////////
//Desc: simple puzzle that runs in water room if player has cube - opens north from waterroom/////////
//Params: p_player - pointer to player////////////////////////////////////////////////////////////////
//Params: p_items - pointer to itemstore - all items in world/////////////////////////////////////////
//Return: boolean - true if puzzle has happened///////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
bool puzzle2(Player* p_player, ItemStore* p_items)
{
	int input = 0;
	std::string stringInput;

	if(p_player->getCurrentPosition()->getName() == "Water Room")
	{
		if(p_player->hasItem(p_items->getByName("cube")))
		{
				cout << "A pillar raised up from the ground, and the cube starts glowing. Hit \"r\" for use item roll. Anything else will quit." << endl;
				stringInput = getStringInput();
				if(stringInput == "r")
				{	
					int rand = rollD100();
					rand+=p_player->useItem();
					if(rand > 50)
					{
						cout << "You place the cube into the centre of the pillar. It raises up in the air and starts to spin with massive force. It glows immensely and the water discipates from the room. It seems the northern door is now accessible." << endl;
						p_player->removeItemFromInventory(p_items->getByName("cube"));
						p_player->getCurrentPosition()->getNorthRoom()->unLock();
						p_player->getCurrentPosition()->setDescription("The water has been obsorbed into the cube. A stone pillar and levitating cube fill the room with dark magic.");
						return true;
					}
					else
					{
						cout << "You can't figure it out." << endl;
					}
				}
				else
				{
					cout << "Quit puzzle" << endl;
				}
		}
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//Name: Battle////////////////////////////////////////////////////////////////////////////////////////
//Desc: A fight happens when you enter a room. Admitadly bad code. Just a bit of fun//////////////////
//Params: p_player - pointer to player////////////////////////////////////////////////////////////////
//Params: p_enemy - pointer opposing enemy////////////////////////////////////////////////////////////
//Return: boolean - true if win fight/////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
bool battle(Player* p_player, Enemy* p_enemy)
{
	//necessary variables
	enum choice{HEAL = 1, ATTACK};
	int playerInit = 0;
	int enemyInit = 0;
	int playerAttack = 0;
	int enemyAttack = 0;
	std::string input;
	int intput = 0;
	int rand;

	//introduction
	cout << "\nA " << p_player->getCurrentPosition()->getEnemy()->getName() << " stands in your way, Get ready to battle.\n" << endl;
	cout << "BATTLE:" << endl;
	//start battle loop
	do
	{
		cout << "\n\nYour HP: " << p_player->getHp() << "\n" << p_enemy->getName() << " HP: " << p_enemy->getHP() << endl;
		cout << "\nRoll for initiative. Hit \"r\", anything else and you sacrifice this chance.\n" << endl;
		input = getStringInput();
		if(input == "r")
		{
			playerInit = rollD100();
			enemyInit = getRand();
		}
		else
		{
			cout << "You get 0 for initiative." << endl;
		}
		//If player has initiative
		if(playerInit >= enemyInit)
		{
			cout << "\nWhat would you like to do? 1)Heal 2)Attack" << endl;
			intput = getInput();
			
			switch(intput)
			{
			case HEAL:
				rand = getRand();
				cout << "You bandage yourself up and heal " << rand << " hits." << endl;
				p_player->setHP(p_player->getHp()+rand);
				break;
			case ATTACK:
				cout << "Roll for your attack. (\"r\") \n>" << endl;
				input = "";
				cin >> input;
				if(input == "r")
				{
					rand = rollD100() + p_player->getOB();
					cout << "\nYou attack the "<< p_enemy->getName() <<" with " << rand << " hits.\n" << endl;
					p_enemy->reduceHP(rand);
				}
				else
				{
					cout << "\nYou fail to attack.\n" << endl;
				}
				break;
			}
			rand = (getRand() + p_enemy->getOB())/4;
			cout << "\nThe " << p_enemy->getName() << " attacks you with " << rand << " hits.\n" << endl;
			p_player->reduceHp(rand);
		}
		else//enemy has initiative 
		{
			rand = (getRand() + p_enemy->getOB())/4;
			cout << "\nThe " << p_enemy->getName() << " attacks you with " << rand << " hits.\n" << endl;
			p_player->reduceHp(rand);

			if(p_player->getHp() <= 0)
			{
				break;
			}

			cout << "Your HP: " << p_player->getHp() << endl;
			cout << "\nWhat would you like to do? 1)Heal 2)Attack" << endl;
			intput = getInput();
			switch(intput)
			{
			case HEAL:
				rand = getRand()/5;
				cout << "\nYou bandage yourself up and heal " << rand << " hits.\n" << endl;
				p_player->setHP(p_player->getHp()+rand);
				break;
			case ATTACK:
				cout << "\nRoll for your attack. (\"r\") \n>" << endl;
				input = "";
				cin >> input;
				if(input == "r")
				{
					rand = rollD100() + p_player->getOB();
					cout << "\nYou attack the "<< p_enemy->getName() <<" with " << rand << " hits." << endl;
					p_enemy->reduceHP(rand);
				}
				else
				{
					cout << "\nYou fail to attack." << endl;
				}
				break;
			}
		}
	}
	while(p_player->getHp() > 0 && p_enemy-> getHP() > 0);//Run while player and enemyare alive.
	
	//fight over. Figure out how it ended

	if(p_player->getHp() < 0)//if you lose
	{
		cout << "\nYou lost the battle." << endl;
		p_player->getCurrentPosition()->setEnemy(0);		
		return false;
	}
	else
	{
		cout << "\nYou won the battle." << endl;
		p_player->addExp(p_enemy->getExpReward());
		p_player->getCurrentPosition()->setEnemy(0);
		return true;
	}
}