#ifndef CONTAINER_H
#define CONTAINER_H
#include "Room.h"
//Container class. Can store other items and be opened too. When opened it will move items into
//The room that holds the container.
class Container : public Item
{
private:
	ItemStore m_contents;
public:
	Container();
	Container(const std::string &p_name, const std::string &p_desc, ItemStore& p_items);
	void look(Room* p_room);
	void removeItem(Item* p_item);
	bool hasItem(Item* p_item);
	void print();
	void save(std::string& filename);
	void load(std::string& filename);
	void read_object(FILE * in);
	void write_object(FILE * out);
};
#endif