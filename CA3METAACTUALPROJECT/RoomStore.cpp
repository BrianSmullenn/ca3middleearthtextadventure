#include "Room.h"
#include <iostream>
using std::string;
using std::cout;
using std::endl;
using std::vector;

RoomStore::RoomStore()
{
	m_list = vector<Room>();
}

void RoomStore::append(Room p_Room)
{
	m_list.push_back(p_Room);
}

//========================================================================
// Name: Print
// Desc: Print that database to screen
// Args: none
// Ret: none
//========================================================================
void RoomStore::print()
{
	enum compass{NORTH = 0, SOUTH, EAST, WEST, UP, DOWN};
	int count = -1;
	cout << "You see ";
	for (vector<Room>::iterator itr = m_list.begin(); itr != m_list.end(); ++itr)
	{
		count++;
		if(itr->getName() != "|Room Name|")
		{
			if(count == 5)
			{
				cout << "and ";
			}
			if(itr->isDiscovered())
			{
				cout << "the " << itr->getName();
			}
			else
			{
				cout << "a door";
			}
			if(count == NORTH)
			{
				cout << " to the north, ";
			}
			if(count == SOUTH)
			{
				cout << " to the south, ";
			}
			if(count == EAST)
			{
				cout << " to the east, ";
			}
			if(count == WEST)
			{
				cout << " to the west, ";
			}
			if(count == UP)
			{
				cout << " above, ";
			}
			if(count == DOWN)
			{
				cout << " below, ";
			}
		}
	}
	cout << "\b\b.";
}

//========================================================================
// Name: Contains
// Desc: check if database contains a player
// Args: p_p - the player to check (also looks like a face)
// Ret: boolean - true if database contains p_p
// Complexity: 0(n)
//========================================================================
bool RoomStore::contains(Room& p_room)
{
	for (vector<Room>::iterator itr = m_list.begin(); itr != m_list.end(); ++itr)
	{
		if(itr->getName() == p_room.getName())
		{
			return true;
		}
	}
	return false;
}

