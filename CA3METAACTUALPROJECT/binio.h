#ifndef UBELTBINIO_H
#define UBELTBINIO_H

//COMPLETELY SHANE GAVINS. JUST WANTED A LOG IN..IS THAT SO BAD..

/**
 * @author  Shane Gavin <shout@nodehead.com> <http://nodehead.com/>
 * @version 0.1
 * @since   0.1
 *
 * This file defines a number of free functions which can be used to perform
 * operations on, and interact with, binary files. These functions are sub-
 * grouped into the ubelt::binio namespace. Given the small amount of
 * functionality within, a using namespace... should be acceptable in most
 * (though not all) circumstances.
 *
 * Copyright (c) Shane Gavin 2014
 *
 * This file is part of the ubelt namespace.
 *
 * the ubelt namespace is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * the ubelt namespace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the ubelt namespace. If not, see <http://www.gnu.org/licenses/>.
 */

//#############################################################################
// Dependencies
//#############################################################################

#include <cstdio>
#include <string>


//#############################################################################
// The Meat
//#############################################################################

namespace ubelt{ namespace binio
{
    // Writing
    template <class T> inline bool write(const T&, FILE *);
                              bool write(const std::string&, FILE *);

    // Reading
    template <class T> inline bool read(T& dest, FILE *);
                              bool read(std::string& dest, FILE *);

    // Movement
    template <class T> inline bool skip(FILE *);
                              bool skip(FILE *);
                       inline bool skip(size_t, FILE *);

    // Info
    inline size_t available(FILE *);
           size_t size(FILE *);




    //#########################################################################
    // Template and inline implementations
    //#########################################################################

    /**
     * Write a single variable to the specified binary file pointer.
     * @param  source Variable to write
     * @param  file   Pointer to file to write to.
     * @return        True if the variable was written, otherwise false.
     */
    template <class T> inline bool write(const T& source, FILE * file)
    {
        return (1 == fwrite(&source, sizeof(T), 1, file));
    }

    /**
     * Read a single variable from the specified binary file pointer.
     * @param  dest Variable to read to
     * @param  file Pointer to file to write to.
     * @return      True if the value could be read, otherwise false
     */
    template <class T> inline bool read(T& dest, FILE * file)
    {
        return (1 == fread(&dest, sizeof(T), 1, file));
    }

    /**
     * Skip an instance (no of bytes) of a single variable in the specified
     * binary file pointer.
     * @param  file ointer to file to skip over value in.
     * @return      True if the value was skipped over, otherwise false.
     */
    template <class T> inline bool skip(FILE * file)
    {
        // 0 indicates a successful seek!
        return (0 == fseek(file, sizeof(T), SEEK_CUR));
    }

    /**
     * Skip n bytes in the specified binary file pointer.
     * @param  bytes The number of bytes to skip over
     * @return       True if the bytes were skipped over, otherwise false.
     */
    inline bool skip(size_t bytes, FILE * file)
    {
        // 0 indicates a successful seek!
        return (0 == fseek(file, bytes, SEEK_CUR));
    }

    /**
     * Find the number of bytes remaining in the specified binary file for
     * reading. <b>WARNING: </b> This is calculated per call, so can be
     * quite expensive. Certainly, its use for iterating over a file's
     * contents would not be advisable.
     *
     * @param  file A binary file pointer
     * @return      The number of bytes left in the file for reading.
     */
    inline size_t available(FILE * file)
    {
        return size(file) - ftell(file);
    }
}}

#endif