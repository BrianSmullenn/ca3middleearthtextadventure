#ifndef QUESTITEM_H
#define QUESTITEM_H
#include "Item.h"
#include "Room.h"
#include "Player.h"
//An extension of item
//needed for puzzles
class QuestItem : public Item
{
private:
	std::string m_use;
public:
	QuestItem();
	QuestItem(const std::string &m_name, const std::string &m_desc, const std::string &p_use);
	void useItem();
	void activate(Player* p_player);
	void print();

	void save(std::string& filename);
	void load(std::string& filename);
	void read_object(FILE * in);
	void write_object(FILE * out);
};
#endif