#define _CRT_SECURE_NO_DEPRECATE
#include "Game.h"
#include "Container.h"
#include "QuestItem.h"
#include "config.h"
#include <cstdio>
using std::endl;
using std::cin;
using std::cout;
using std::string;
using std::vector;
using std::list;

// MSVC memory-leak-checking code
#ifdef _MSC_VER
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

int main()
{
	// MSVC memory-leak-checking code
    #ifdef _MSC_VER
		#if defined(DEBUG) | defined(_DEBUG)
            _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
        #endif
    #endif
	
	vector<Room*> rooms;
	rooms = setUpWorld();

	vector<Enemy*> enemies;
	enemies = setUpEnemies(rooms);

	ItemStore items;
	items = addItemsToWorld(rooms);//0,4,5,6 regularItems
	
	
	//items[2]->save(filename);
	//Item* test = new Container();
	//test->load(filename);
	//test->print();
	//items.save(filename);

	//string filename = ".\\items.txt";
	//items2->load(filename);
	
	//items.load(filename);
	//items.print();
	//items[0]->save();
	
	//items.load(filename);
	//cout << items[7]->save(filename);
	//test.print();
	//test.save();
	//test.load();
	//cout << test.size() << endl;
	//test.print();
	//cout << test[1]->getDesc() << endl;
	printro();
	Player p1;

	//p1.setUp();
	//cout << "SIGN IN ENTER CHARACTE NAME" << endl;
	//string characterName;
	//cin >> characterName;
	//string filename = "./Brian.txt";
	//FILE* outfile = fopen(filename.c_str(), "wb");
	//FILE* infile = fopen(filename.c_str(), "rb");
	//p1.write_object(outfile);
	//p1.read_object(infile);

	p1.start();

	p1.setCurrentPosition(rooms.at(10));//This would seem to be working so far.
	if(p1.getName() == "Mario")
	{
		p1.setRace("Italian");
		p1.setClass("Plumber");
	}
	gameLoop(rooms, items, p1);
	
	
	cleanUp(&items, &rooms, &enemies);
	items.cleanUp();

	cin.ignore();
	return 0;
}