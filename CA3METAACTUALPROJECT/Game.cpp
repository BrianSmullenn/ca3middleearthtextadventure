//////////////////////////////////////////////////////////////////////////////////////////////////////
//Implemenation of game functions/////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////

#include "Game.h"
#include "QuestItem.h"
#include "Container.h"
#include "puzzles.h"
#include <iostream>

using std::vector;
using std::list;
using std::cout;
using std::endl;
using std::cin;
using std::string;

//////////////////////////////////////////////////////////////////////////////////////////////////////
//Name: GetStringInput////////////////////////////////////////////////////////////////////////////////
//Desc: get an input string from the user/////////////////////////////////////////////////////////////
//Params: none////////////////////////////////////////////////////////////////////////////////////////
//Return: input - player input////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
string getStringInput()
{
	cout << endl;
	cout << ">";
	string input;
	getline(cin, input);
	return input;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//Name: GetInput//////////////////////////////////////////////////////////////////////////////////////
//Desc: get an input integer from the user////////////////////////////////////////////////////////////
//Params: none////////////////////////////////////////////////////////////////////////////////////////
//Return: input - player input////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
int getInput()
{
	cout << endl;
	cout << ">";
	int input;
	while(!(cin >> input))
	{
		cout << "Invalid, try again:" << endl;
		cin.clear();
		cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	}
	cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	return input;
}

int getInput(int p_min, int p_max)
{
	cout << endl;
	cout << ">";
	int input;

	while (!(cin >> input) || input < p_min || input > p_max)
	{
		cout << "Invalid, please re-enter: \n";
		cin.clear();
		cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	}
	cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	return input;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//Name: Printro///////////////////////////////////////////////////////////////////////////////////////
//Desc: print the intro to he game////////////////////////////////////////////////////////////////////
//Params: none////////////////////////////////////////////////////////////////////////////////////////
//Return: none////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
void printro()
{
	cout << "================================================================================" <<
			"====================================META========================================" <<
			"=====================Middle Earth Text Adventure: Episode 3=====================" <<
			"================================================================================\n" <<
			"You're on a quest to find \"Coin Spinner\", one of the 12 swords of power. " <<
			"The wicked Lord Aran is waiting with the promise of wealth, health and fortune. " <<
			"You arrive in the crypt of dunedine kings atlast. It seems the hardest bit has passed. " <<
			"The entrance slams shut behind you. \"I guess the only way out is at the end, with this infamous sword.\"\n\n "
			<< endl;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//Name: PrintOutro////////////////////////////////////////////////////////////////////////////////////
//Desc: Print outro of the game///////////////////////////////////////////////////////////////////////
//Params: none////////////////////////////////////////////////////////////////////////////////////////
//Return: none////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
void printOutro()
{
	cout << 
	"\n\nYou take Coin Spinner and the cieling starts to crumble around you. Wether you keep the sword or return it to the king" <<
	"is up to you but one things for sure you gotta get out of here. With Coin Spinner in had you sprint towards the entrance " <<
	"and out into the daylight. As the tomb is destroyed behind, Light blinds you and all you can hear is a fierce wind escaping the tomb.\n\n" <<
	"\n\nto be continued....." << 
	"================================================================================" <<
	"====================================THE END=====================================" <<
	"=============================Thank you for playing==============================" <<
	"===============================By: Brian Smullen================================" <<
	"================================================================================" << endl;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//Name: setUpWorld////////////////////////////////////////////////////////////////////////////////////
//Desc: set up rooms in the world create each, link each, and add to p_rooms//////////////////////////
//Params: none////////////////////////////////////////////////////////////////////////////////////////
//Return: rooms - all rooms in world//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
vector<Room*> setUpWorld()
{
	enum directions{NORTH,SOUTH,EAST,WEST,UP,DOWN};
	vector<Room*> rooms;
	string input;

	Room* blank = new Room("default","default");
	Room* ThroneRoom = new Room("Throne Room", "A dusty red carped leads up to a golden throne. A luxurious staircase leads up behind the throne. The path is blocked though from the fallen ceiling. Pillars and flags stretch right to whats left of the roof. Light pours in from outside.");
	Room* WaterRoom = new Room("Water Room", "This room is filled with water. It doesnt seem like normal water though. You move through with ease, You can breath like normal too. Maybe an illusion or dark magic. Theres a fountain in the centre with a square hatch in the centre.");
	Room* FireRoom = new Room("Fire Room", "A huge furnace spits fire out filling the room. It doesn't seem to burn your skin though. The furnishings seem to be fine too.");
	Room* TreasureRoom = new Room("Treasure Room", "A room filled entirely with gems and gold. As you walk through the room, every bit of it dissapears.");
	Room* Hall = new Room("Hall One", "This is a long corridor. Stone hands seem to be reaching out from the walls.");
	Room* Hall2 = new Room("Hall Two", "In this hallway, mounted torches burn with a blue liquid flame. Strange.");
	Room* StatueRoom = new Room("Statue Room", "Huge statues of vampires and warewolves face eachother, they look like there about to battle.");
	Room* Library = new Room("Library", "This seems to be a library, Ancient books line the walls, on towering shelving.");
	Room* AlterRoom = new Room("Alter Room", "A huge stone alter to the north of the room. There is a really cold aura in this room.");
	Room* Dungeon = new Room("Dungeon", "This is clearly a dungeon. Torture devices lay derelict, decayed bodies along side. Scrawles on the wall in blood is 'Once more, A curse released, an open door'.");
	Room* EntrancHall = new Room("Entrance Hall", "This is the entrance to this crypt. Solid soldiers with master crafted armour and weapons stand in a circle around an empty and damaged sarcophagus.");
	FireRoom->lock();
	Hall2->lock();
	ThroneRoom->addNearRooms(blank, Hall2, blank, blank, blank, blank);
	WaterRoom->addNearRooms(Hall2, blank, Hall, blank, blank, blank);
	FireRoom->addNearRooms(blank, StatueRoom, blank, Hall, blank, blank);
	TreasureRoom->addNearRooms(blank, blank, blank, StatueRoom, blank, blank);
	Hall->addNearRooms(blank, blank, FireRoom, WaterRoom, blank, blank);
	Hall2->addNearRooms(ThroneRoom, WaterRoom, blank, blank, blank, blank);
	StatueRoom->addNearRooms(FireRoom, EntrancHall, TreasureRoom, Library, blank, blank);
	Library->addNearRooms(blank, blank, StatueRoom, blank, blank, blank);
	AlterRoom->addNearRooms(blank, blank, EntrancHall, blank, blank, Dungeon);
	Dungeon->addNearRooms(blank, blank, blank, blank, AlterRoom, blank);
	EntrancHall->addNearRooms(StatueRoom, blank, blank, AlterRoom, blank, blank);
	
	
	rooms.push_back(ThroneRoom);
	rooms.push_back(WaterRoom);
	rooms.push_back(FireRoom);
	rooms.push_back(TreasureRoom);
	rooms.push_back(Hall);
	rooms.push_back(Hall2);		
	rooms.push_back(StatueRoom);
	rooms.push_back(Library);
	rooms.push_back(AlterRoom);
	rooms.push_back(Dungeon);
	rooms.push_back(EntrancHall);
	rooms.push_back(blank);

	//Set up a few enemies in game to battle
	/*Enemy* vamp = new Enemy("Master Vampire","","A vicious blow to the head splatters the vampire.","", 30,200,30,20, 200);
	FireRoom->setEnemy(vamp);
	Enemy* priest = new Enemy("Apprentice Priest","","Black robes from head to toe. Evil as can be.","", 40,70,20,20, 100);
	AlterRoom->setEnemy(priest);
	Enemy* zomb = new Enemy("Dunedine King","","A giant rotted skeleton. Must have been here for years.","", 35,400,40,20,300);
	ThroneRoom->setEnemy(zomb);*/

	return rooms;
}

vector<Enemy*> setUpEnemies(vector<Room*> rooms)
{
	vector<Enemy*> enemies;
	
	Enemy* vamp = new Enemy("Master Vampire","","A vicious blow to the head splatters the vampire.","", 30,200,30,20, 200);
	rooms[2]->setEnemy(vamp);
	Enemy* priest = new Enemy("Apprentice Priest","","Black robes from head to toe. Evil as can be.","", 40,70,20,20, 100);
	rooms[8]->setEnemy(priest);
	Enemy* zomb = new Enemy("Dunedine King","","A giant rotted skeleton. Must have been here for years.","", 35,400,40,20,300);
	rooms[0]->setEnemy(zomb);
	enemies.push_back(vamp);
	enemies.push_back(priest);
	enemies.push_back(zomb);
	return enemies;

}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//Name: AddItemsToWorld///////////////////////////////////////////////////////////////////////////////
//Desc: set up items and add to rooms/////////////////////////////////////////////////////////////////
//Params: none////////////////////////////////////////////////////////////////////////////////////////
//Return: items - ItemStore of items in world/////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
ItemStore addItemsToWorld(vector<Room*> p_rooms)
{
	ItemStore items;
	Item* i1 = new Item("rope", "A thick, well braided rope.");
	Item* i2 = new QuestItem("ceremonial knife","This is a sharp blade, grooves are cut into the side and some strange writing runs up the handle.","You cut your hand with the knife. Your blood runs through the grooves and the writing starts to glow. There is an evil magic in this knife.");
	Item* i3 = new Weapon("sword","A stunning, master crafted sword. signed 'Brynjolf' on the glistening steel.",10);
	Item* i4 = new QuestItem("cube", "The cube is glowing blue, some strange writing is carved into the side.", "USE");
	Item* i5 = new Item("ring", "A shiny ring, with a beautiful blue saphire.");
	Item* i6 = new Item("shoes", "A pretty fancy pair of shoes. Not much use though.");
	Item* i7 = new Item("cat", "This cat has been dead for quite a while.");
	Item* i8 = new QuestItem("Coin Spinner", "The infamous sword of power. Legend says \"It'll bring massive riches to its wielder\", sounds fun.", "You cant use this.");
	Item* i9 = new QuestItem("stone chalice","A large stone chalice, Stained with old blood. You feel an evil presence looking into this."," A use or something");
	Item* i10 = new Item("rope", "A thick, well braided rope.");
	Item* i11 = new Item("diamond", "It's flawless. maybe this will be worth something.");
	Item* i12 = new Item("hammer", "Just an old hammer.");
	Item* i13 = new Item("helmet", "An old helmet. Its pretty worn.");
	Item* i14 = new Item("cheese", "Theres always cheese in these games.");
	Item* i15 = new Item("apple", "Always apples in rpgs too.");
	Item* i16 = new Item("ming-vase", "Its priceless, seems familiar.");
	Item* i17 = new Item("Lute", "Its a nice instrument but I dont see any need for it, not yet.");
	Item* i18 = new Item("note", "It says: Bring the chalice and ceremonial knife to statue room.");
	Item* i19 = new Item("scrap paper", "You cant make out much but it seems to say. Cube and water.");
	Item* i20 = new Item("Herpes","You've contracted herpes, Thats fo life.");
	Item* w2 = new Weapon("War Axe", "It's a monster, Takes two hands to lift", 30);
	Item* w3 = new Weapon("Needle Sword", "Lots of people name their swords, lots of C##!S", 20);
	ItemStore c1Items;
	c1Items.append(i5);
	c1Items.append(i6);
	c1Items.append(i7);
	Item*c1 = new Container("chest","A small wooden chest. Try the 'open' command.",c1Items);
	items.append(i1);
	items.append(i2);
	items.append(i3);
	items.append(i4);
	items.append(i5);
	items.append(i6);
	items.append(i7);
	items.append(c1);
	items.append(i8);
	items.append(i9);
	items.append(i10);
	items.append(i11);
	items.append(i12);
	items.append(i13);
	items.append(i14);
	items.append(i15);
	items.append(i16);
	items.append(i17);
	items.append(i18);
	items.append(i19);
	items.append(i20);
	items.append(w2);
	items.append(w3);

	//add to rooms
	p_rooms[9]->addItemToStore(items[0]);
	p_rooms[8]->addItemToStore(items[1]);
	p_rooms[7]->addItemToStore(items[3]);
	p_rooms[3]->addItemToStore(items[7]);
	p_rooms[0]->addItemToStore(items.getByName("Coin Spinner"));
	p_rooms[9]->addItemToStore(items.getByName("stone chalice"));
	p_rooms[2]->addItemToStore(items[10]);
	p_rooms[3]->addItemToStore(items[11]);
	p_rooms[1]->addItemToStore(items[12]);
	p_rooms[4]->addItemToStore(items[13]);
	p_rooms[6]->addItemToStore(items[14]);
	p_rooms[8]->addItemToStore(items[15]);
	p_rooms[9]->addItemToStore(items[16]);
	p_rooms[1]->addItemToStore(items[17]);
	p_rooms[8]->addItemToStore(items[18]);
	p_rooms[10]->addItemToStore(items[19]);
	p_rooms[4]->addItemToStore(items[20]);
	p_rooms[7]->addItemToStore(items[21]);
	p_rooms[9]->addItemToStore(items[22]);
	return items;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//Name: parse/////////////////////////////////////////////////////////////////////////////////////////
//Desc: parse the players input. What do they want to do and how do i do it?//////////////////////////
//Params: p_input - player input to parse/////////////////////////////////////////////////////////////
//Params: p_player - the player object - generally affected by the parsing////////////////////////////
//Params: p_items - items may be needed here//////////////////////////////////////////////////////////
//Return: none////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
void parse(std::string &p_input, Player &p_player, ItemStore p_items)
{	
	int NORTH = 1;
	int SOUTH = 2;
	int EAST = 3;
	int WEST = 4;
	int UP = 5;
	int DOWN = 6;
	
	string firstWord = p_input.substr(0, p_input.find(' '));
	string secondWord = p_input.erase(0, firstWord.length() +1);
	//string secondWord = p_input.erase(0, eraseto); // code from http://www.cplusplus.com/reference/string/string/erase/

	if(firstWord == "North" || firstWord == "north" || firstWord == "n" || firstWord == "N")
	{		
		if(!(p_player.getCurrentPosition()->getNorthRoom()->exists()))
		{
			cout << "\nYou can't go that way.\n" << endl;
			return;
		}
		
		if(!(p_player.getCurrentPosition()->getNorthRoom()->isLocked()))
		{
			p_player.setCurrentPosition(p_player.getCurrentPosition()->getNorthRoom());
			return;
		}
		else
		{ 
			cout << "This room is locked. \n" << endl;
			return;
		}
	}
	if(firstWord == "South" || firstWord == "south" || firstWord == "s" || firstWord == "S")
	{		
		if(!(p_player.getCurrentPosition()->getSouthRoom()->exists()))
		{
			cout << "\nYou can't go that way.\n" << endl;
			return;
		}
		
		if(!(p_player.getCurrentPosition()->getSouthRoom()->isLocked()))
		{
			p_player.setCurrentPosition(p_player.getCurrentPosition()->getSouthRoom());
			return;
		}
		else
		{ 
			cout << "This room is locked. \n" << endl;
			return;
		}
	}
	if(firstWord == "East" || firstWord == "east" || firstWord == "e" || firstWord == "E")
	{		
		if(!(p_player.getCurrentPosition()->getEastRoom()->exists()))
		{
			cout << "\nYou can't go that way.\n" << endl;
			return;
		}
		
		if(!(p_player.getCurrentPosition()->getEastRoom()->isLocked()))
		{
			p_player.setCurrentPosition(p_player.getCurrentPosition()->getEastRoom());
			return;
		}
		else
		{ 
			cout << "This room is locked. \n" << endl;
			return;
		}
	}
	if(firstWord == "West" || firstWord == "west" || firstWord == "w" || firstWord == "W")
	{		
		if(!(p_player.getCurrentPosition()->getWestRoom()->exists()))
		{
			cout << "\nYou can't go that way.\n" << endl;
			return;
		}
		
		if(!(p_player.getCurrentPosition()->getWestRoom()->isLocked()))
		{
			p_player.setCurrentPosition(p_player.getCurrentPosition()->getWestRoom());
			return;
		}
		else
		{ 
			cout << "This room is locked. \n" << endl;
			return;
		}
	}
	if(firstWord == "Up" || firstWord == "up" || firstWord == "u" || firstWord == "U")
	{		
		if(!(p_player.getCurrentPosition()->getUpperRoom()->exists()))
		{
			cout << "\nYou can't go that way.\n" << endl;
			return;
		}
		
		if(!(p_player.getCurrentPosition()->getUpperRoom()->isLocked()))
		{
			p_player.setCurrentPosition(p_player.getCurrentPosition()->getUpperRoom());
			return;
		}
		else
		{ 
			cout << "This room is locked. \n" << endl;
			return;
		}
	}
	if(firstWord == "Down" || firstWord == "down" || firstWord == "d" || firstWord == "D")
	{		
		if(!(p_player.getCurrentPosition()->getLowerRoom()->exists()))
		{
			cout << "\nYou can't go that way.\n" << endl;
			return;
		}
		
		if(!(p_player.getCurrentPosition()->getLowerRoom()->isLocked()))
		{
			p_player.setCurrentPosition(p_player.getCurrentPosition()->getLowerRoom());
			return;
		}
		else
		{ 
			cout << "This room is locked. \n" << endl;
			return;
		}
	}
	if(firstWord == "Open" || firstWord == "open")
	{
		if(p_player.getCurrentPosition()->hasItem(p_items.getByName(secondWord)))
		{//check if has item with secondword as name
			p_items.getByName(secondWord)->look(p_player.getCurrentPosition());
		}

		//cout << secondWord << endl;
		return;
	}
	if(firstWord == "Take" || firstWord == "take")
	{
		if(p_player.getCurrentPosition()->hasItem(p_items.getByName(secondWord)))
		{
			p_player.addItemToInventory(p_items.getByName(secondWord));
			cout << "Added " << secondWord << " to your inventory." << endl; 
			p_player.getCurrentPosition()->removeItem(p_items.getByName(secondWord));
		}
		else
		{
			cout << "There is no " << secondWord << " here" << endl;
		}
		//remove from room.
	}
	if(firstWord == "i" || firstWord == "inv")
	{
		cout << "==========Inventory===========" << endl;
		p_player.printInventory();
	}
	if(firstWord == "r" || firstWord == "roll")
	{
		rollD100();
	}
	if(firstWord == "me" || firstWord == "Me")
	{
		p_player.print();
	}
	if(firstWord == "save" || firstWord == "Save")
	{
		cout << "Rose: You remember what day it is Jack?\n>" << endl;
		cin.ignore();
		p_player.save();
	}
	if(firstWord == "x" || firstWord == "ex" || firstWord == "X" || firstWord == "examine")
	{
		if(p_player.hasItem(p_items.getByName(secondWord)))
		{
			cout << p_items.getByName(secondWord)->getDesc() << endl;
		}
		else
		{
			cout << "Try picking up an item then examine" << endl;
		}
	}
	if(firstWord == "eq" || firstWord == "equip" || firstWord == "Equip")
	{
		/*if(p_items.getByName(secondWord)->getName() == "NeedleSword")
		{
			p_player.equip(p_items[21]);
		}*/
		cout << "Equip not supported just yet." << endl;
	}
	if(firstWord == "Help"||firstWord == "help"||firstWord == "controls"||firstWord == "c")
	{
		printControls();
	}

}

void printControls()
{
	cout << "============Controls=================\n" <<
			"n,north - Movement(n,s,e,w,u,d)\n"	<<
			"take 'full case sensitive item name' - take \n" <<
			"save - save player stats\n" <<
			"me - view stats\n" <<
			"r - roll dice\n" <<
			"i - view inventory\n" <<
			"x or examine - examine 'full case sensitive item name'\n" <<
			"====================================\n" << endl;

}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//Name: GameLoop//////////////////////////////////////////////////////////////////////////////////////
//Desc: The main game loop - print locaton check for puzzles or battle get input and parse////////////
//      run while not stop////////////////////////////////////////////////////////////////////////////
//Params: p_rooms - rooms need to be pased through here///////////////////////////////////////////////
//Params: p_items/////////////////////////////////////////////////////////////////////////////////////
//Params: p_player - the player///////////////////////////////////////////////////////////////////////
//Return: none////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
int gameLoop(vector<Room*> p_rooms, ItemStore p_items, Player& p_player)
{
	bool stop = false;
	string input;
	do
	{
		if(p_player.getCurrentPosition()->hasEnemy())
		{
			if(!battle(&p_player, p_player.getCurrentPosition()->getEnemy()))
			{
				cout << "===============GAME OVER================" <<
						"\n==Snake?...Snake?...SNNAAKKEE!!!! beep==" << endl;
				stop = true;	
				break;
			}
		}
		{
		if(p_player.hasItem(p_items.getByName("Coin Spinner")))
		{
			if(p_player.getName() != "Mario")
			{
				printOutro();
			}
			else
			{
				cout << "\n\nYou pick up Coin Spinner and it disappears into nothing. Bowser descends all magical and shit. He yells at the top of his dinasaur/big lizard lungs \"Sorry Mario\" Your princess is in another castle, in another game on a different platform. Thanks for playing though." << endl;
			}
			
			stop = true;
			break;
		}
		puzzle1(&p_player, &p_items);
		puzzle2(&p_player, &p_items);
		
		
		p_player.getCurrentPosition()->discover();
		p_player.getCurrentPosition()->print();
		
		input = getStringInput();
		
		if(input != "exit")
		{
			parse(input, p_player, p_items);
		}
		else
		{
			stop = true;
			cout << "Exiting" << endl;
		}
	}
	}while(!stop);
	return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//Name: rollD100//////////////////////////////////////////////////////////////////////////////////////
//Desc: simulate a dice roll, gets a random number and also prints out random numbers just for fun////
//Return: rand - dice number//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
int rollD100()
{
	for(int i = 0; i != 10; i++)
	{
		cout << getRand() << endl;
	}
	int rand = getRand();
	cout << rand << endl;
	cout << "You roll " << rand << endl;
	return rand;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//Name: getRand///////////////////////////////////////////////////////////////////////////////////////
//Desc: get a random number between 1 and 100/////////////////////////////////////////////////////////
//Return: random number///////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
int getRand()
{
	return std::rand()%100+1;
}

void cleanUp(ItemStore* p_items, vector<Room*>* p_rooms, vector<Enemy*>* p_enemies)
{
	for(vector<Room*>::iterator itr = p_rooms->begin(); itr != p_rooms->end(); itr++)
	{
		delete (*itr);
	}
	
	for(vector<Enemy*>::iterator itr = p_enemies->begin(); itr != p_enemies->end(); itr++)
	{
		delete (*itr);
	}
}