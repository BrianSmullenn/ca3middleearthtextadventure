#ifndef PLAYER_H
#define PLAYER_H
#include "Room.h"
#include "Weapon.h"

class Player
{
private:
	Room* m_currentpos;
	ItemStore m_inventory;
	Weapon* m_equipedweapon;
	
	std::string m_name;
	std::string m_password;
	std::string m_race;
	std::string m_class;

	int m_hp;
	int m_ob;
	int m_db;
	int m_mm;
	int m_perception;
	int m_detectTrap;
	int m_useItem;

	int m_strength;
	int m_agility;
	int m_intelligence;
	int m_intuition;
	int m_presence;

	int m_skillPoints;

	int m_exp;
	int m_lvl;

	int m_max_HP;

	void assignRaceStats(int &input);
	void assignClassStats(int &input);
	void assignBonuses();
	void assignSkillpoints();

	

public:
	Player();
	void setUp();
	void setCurrentPosition(Room* p_room);
	Room* getCurrentPosition();
	void print();
	void initialise();
	void bumpSkill(int skill, int amount);
	int getExp();
	void addExp(int p_exp);
	int getLvl();
	void setLvl();
	void setHP(int p_hp);
	int getHp();
	void reduceHp(int p_damage);
	int getOB();

	void equip(Weapon* p_weapon);

	std::string getName();
	std::string getPassword();

	void setClass(const std::string& p_class);
	void setRace(const std::string& p_class);

	int perception();
	int useItem();
	//Inventory specific
	bool hasItem(Item* p_item);
	void addItemToInventory(Item* p_item);
	void removeItemFromInventory(Item* p_item);
	void printInventory();

	bool read_object(FILE * in);
	bool write_object(FILE * out);

	bool save();
	bool load();

	void start();
	bool login();
	void signup();
};
#endif