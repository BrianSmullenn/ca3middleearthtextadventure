#define _CRT_SECURE_NO_DEPRECATE
#include "Player.h"
#include "Game.h"
#include <iostream>
#include "binio.h"

using std::cout;
using std::endl;
using std::cin;
using std::string;

using ubelt::binio::read;
using ubelt::binio::write;

Player::Player()
{
	m_currentpos;

	m_name = "b";
	m_password = "";
	m_race = "";
	m_class = "";

	m_hp = 0;
	m_ob = 0;
	m_db = 0;
	m_mm = 0;
	m_perception = 0;
	m_detectTrap = 0;
	m_useItem = 0;
		
	m_strength = 50;
	m_agility = 50;
	m_intelligence = 50;
	m_intuition = 50;
	m_presence = 50;

	m_skillPoints = 15;

	m_exp = 0;
	m_lvl = 1;
}

void Player::setCurrentPosition(Room *p_room)
{
	m_currentpos = p_room;
}

void Player::setUp()
{
	int input;
	cout << "Enter your player name." << endl;
	m_name = getStringInput();	
	cout << "\n\nEnter a password." << endl;
	m_password = getStringInput();
	cout << "\n" << endl;
	cout << "Choose a race. \n\n1)Elf 2)Human 3) Dwarf 4) Halfling\n" << endl;
	input = getInput();
	assignRaceStats(input);
	cout << "\n" << endl;
	cout << "Choose a class. \n\n1)Fighter 2)Ranger 3) Thief 4) Bard\n" << endl;
	input = getInput();
	assignClassStats(input);
	assignBonuses();
	
	cout << endl;
	print();
	cout<< endl;
	assignSkillpoints();
	
	print();
	m_max_HP = m_hp;
}

void Player::assignSkillpoints()
{
	int skill = 0;
	int amount = 0;

	do
	{
		cout << "You have " << m_skillPoints << " skillpoints to spend where you like." << endl;
		cout << "\n\nChoose a skill: 1)HP 2)OB 3)DB 4)MM 5)Perception 6)Detect Trap 7)Use Item" << endl;
		skill = getInput();
		cout << "How many points?\n" << endl;
		amount = getInput();
		if(amount > m_skillPoints)
		{
			amount = m_skillPoints;
		}
		if(amount > 0)
		{
			bumpSkill(skill, amount);
			m_skillPoints -= amount;
		}
	} while (m_skillPoints > 0);
}

void Player::assignRaceStats(int &p_race)
{
	enum races{ELF = 1, HUMAN, DWARF, HALFLING};
	if(p_race == ELF)
	{
		m_race = "Elf";
		cout << "Elf selected" << endl;
		m_strength += 20;
		m_agility += 40;
		m_intelligence += 50;
		m_intuition += 40;
		m_presence += 10;
		return;
	}
	if(p_race == HUMAN)
	{
		m_race = "Human";
		cout << "Human selected" << endl;
		m_strength += 30;
		m_agility += 30;
		m_intelligence += 30;
		m_intuition += 30;
		m_presence += 30;
		return;
	}
	if(p_race == DWARF)
	{
		m_race = "Dwarf";
		cout << "Dwarf Selected" << endl;
		m_strength += 100;
		m_agility += 50;
		return;
	}
	if(p_race == HALFLING)
	{
		m_race = "Halfling";
		cout << "Halfling Selected" << endl;
		m_strength += 10;
		m_agility += 50;
		m_intelligence += 50;
		m_intuition += 30;
		m_presence += 10;
		return;
	}
}


void Player::assignClassStats(int &p_race)
{
	enum classes{FIGHTER = 1, RANGER, THIEF, BARD};
	if(p_race == FIGHTER)
	{
		m_class = "Fighter";
		cout << "Fighter Selected" << endl;
		m_hp += 40;
		m_ob += 25;
		m_db += 35;
		m_perception -= 25;
		m_detectTrap -= 15;
		m_useItem -= 25;
		return;
	}
	if(p_race == RANGER)
	{
		m_class = "Ranger";
		cout << "Ranger Selected" << endl;
		m_hp += 20;
		m_ob += 10;
		m_db += 25;
		m_mm += 30;
		m_perception += 30;
		m_detectTrap += 10;
		m_useItem += 5;
		return;
	}
	if(p_race == THIEF)
	{
		m_class = "Thief";
		cout << "Thief Selected" << endl;
		m_hp += 20;
		m_db += 25;
		m_ob += 20;
		m_mm += 35;
		m_perception += 30;
		m_detectTrap += 40;
		m_useItem += 30;
		return;
	}
	if(p_race == BARD)
	{
		m_class = "Bard";
		cout << "Bard Selected" << endl;
		m_hp += 40;
		m_ob += 10;
		m_db += 40;
		m_mm += 10;
		m_perception += 30;
		m_detectTrap += 15;
		m_useItem += 15;
		return;
	}
}

void Player::assignBonuses()
{
	if(m_strength >= 70 && m_strength <= 89)
	{
		m_ob += 5;
		m_db += 5;
	}
	else if(m_strength >= 90)
	{
		m_ob += 10;
		m_db += 10;
		m_hp += 5;
	}

	if(m_agility >= 70 && m_agility <= 89)
	{
		m_mm += 5;
	}
	else if(m_agility >= 90)
	{
		m_mm += 15;
	}
	
	if(m_intelligence >= 70 && m_intelligence <= 89)
	{
		m_useItem += 5;
	}
	else if(m_intelligence >= 90)
	{
		m_useItem += 15;
	}

	if(m_intuition >= 70 && m_intuition <= 89)
	{
		m_perception += 5;
	}
	else if(m_intuition >= 90)
	{
		m_perception += 15;
	}

	if(m_presence >= 70 && m_presence <= 89)
	{
		m_detectTrap += 5;
	}
	else if(m_presence >= 70)
	{
		m_detectTrap += 15;
	}
}

void Player::print()
{
	cout << "Name: "<< m_name << endl;
	cout << "Race: "<< m_race << endl;
	cout << "Class: "<< m_class << endl;
	cout << "Stats: " << endl;
	cout << "	Strength: " << m_strength << endl;
	cout << "	Agility: " << m_agility << endl;
	cout << "	Intelligence: " << m_intelligence << endl;
	cout << "	Intuition: " << m_intuition << endl;
	cout << "	Presence: " << m_presence << endl;
	cout << endl;
	cout << "	HP: " << m_hp << endl;
	cout << "	OB: " << m_ob << endl;
	cout << "	DB: " << m_db << endl;
	cout << "	MM: " << m_mm << endl;
	cout << "	Perception: " << m_hp << endl;
	cout << "	Detect Trap: " << m_ob << endl;
	cout << "	Use Item: " << m_db << endl;
}

void Player::bumpSkill(int p_skill, int p_amount)
{
	enum skill{HP = 1, OB, DB, MM, PERCEPTION, DETECTTRAP, USEITEM};
	switch(p_skill)
	{
	case HP:
		m_hp += p_amount;
		break;
	case OB:
		m_ob += p_amount;
		break;
	case DB:
		m_db += p_amount;
		break;
	case MM:
		m_mm += p_amount;
		break;
	case PERCEPTION:
		m_perception += p_amount;
		break;
	case DETECTTRAP:
		m_detectTrap += p_amount;
		break;
	case USEITEM:
		m_useItem += p_amount;
		break;
	}
	return;
}

Room* Player::getCurrentPosition()
{
	return m_currentpos;
}

int Player::getExp()
{
	return m_exp;
}

int Player::getLvl()
{
	return m_lvl;
}

void Player::addExp(int p_exp)
{
	m_exp += p_exp;
	if(m_exp / 100 > m_lvl)
	{
		cout << "LEVEL UP: You are now Level: " << m_exp / 100 << endl;
		m_skillPoints += 10;
		assignSkillpoints();
	}
}

void Player::setLvl()
{
	m_lvl += m_exp / 100;
}

bool Player::hasItem(Item* p_item)
{
	return m_inventory.contains(p_item);
}

void Player::addItemToInventory(Item* p_item)
{
	m_inventory.append(p_item);
}

void Player::removeItemFromInventory(Item* p_item)
{
	m_inventory.remove(p_item);
}

int Player::perception()
{
	return m_perception;
}

int Player::useItem()
{
	return m_useItem;
}

void Player::printInventory()
{
	m_inventory.print();
}

void Player::setClass(const string& p_class)
{
	m_class = p_class;
}

void Player::setRace(const string& p_race)
{
	m_race = p_race;
}
void Player::reduceHp(int p_damage)
{
	if(p_damage > m_db)
	{
		m_hp -= (p_damage-m_db);
		cout << "You take " << (p_damage-m_db) << " hits." << endl;
	}
	else
	{
		cout << "No effect on you." << endl;
	}
}

int Player::getHp()
{
	return m_hp;
}

int Player::getOB()
{
	return m_ob;
}

void Player::setHP(int p_hp)
{
	if(p_hp > m_max_HP)
	{
		cout << "Max HP restored" << endl;
		p_hp = m_max_HP;
		m_hp = p_hp;
	}
	else
	{
		m_hp = p_hp;
	}
}

string Player::getName()
{
	return m_name;
}

string Player::getPassword()
{
	return m_password;
}

void Player::equip(Weapon* p_weapon)
{
	m_ob -= m_equipedweapon->getDamage();
	m_equipedweapon = p_weapon;
	m_ob += m_equipedweapon->getDamage();
}

bool Player::login()
{
	string input;
	load();
	cout << getPassword() << endl;
	bool stop = false;
	do
	{
		cout << "Enter password (enter -1 to exit)" << endl;
		input = getStringInput();
	
		if(input == getPassword())
		{
			return true;
		}
		else if(input == getPassword())
		{
			stop = true;
		}
	}while (!stop);
	return false;
}

void Player::start()
{
	int intput = 0;
	cout << "Would you like to: 1) signup 2) login" << endl;
		intput = getInput(1,2);

	enum choice{SIGNUP = 1, LOGIN};
	switch(intput)
	{
	case SIGNUP:
		signup();
		break;
	case LOGIN:
		if(!login())
		{
			signup();
		}
	}
}

void Player::signup()
{
	setUp();
	save();
}

bool Player::save()
{
	cout << "Enter a name for your save file" << endl;
	string filename = ".\\" + getStringInput() + ".txt";
	FILE* outfile = fopen(filename.c_str(), "wb");
	return write_object(outfile);
}

bool Player::load()
{
	string input;
	cout << "Please enter your save file name." << endl;
	input = getStringInput();

	string filename = "./" + input + ".txt";
	FILE* infile = fopen(filename.c_str(), "rb");
	return read_object(infile);
}
//#############################################################################
// Serialization
//#############################################################################

//THIS CODE BELONGS TO SHANE GAVIN I DO NOT EXPECT ANY MARKS. 
//JUST NEEDED IT SO I WOULDNT HAVE TO CREATE A CHARACTER EVERY TIME

/**
 * Write the current object contents to the specified pointer.
 * @param  out Pointer to an output file; positioned at the desired location.
 * @return     True if the object successfully wrote; otherwise false. 
 */
bool Player::write_object(FILE * out)
{
    return (
        write(this->m_name,	     out) &&
		write(this->m_password,   out) &&
        write(this->m_race,       out) &&
        write(this->m_class,      out) &&
        write(this->m_hp,   out) &&
        write(this->m_ob,      out) &&
        write(this->m_db,  out) &&
        write(this->m_mm,    out) &&
        write(this->m_perception, out) &&
		write(this->m_detectTrap,      out) &&
        write(this->m_useItem,  out) &&
        write(this->m_strength,    out) &&
        write(this->m_agility, out) &&
		write(this->m_intelligence, out) &&
		write(this->m_intuition,      out) &&
        write(this->m_presence,  out) &&
        write(this->m_skillPoints,    out) &&
        write(this->m_exp, out) &&
        write(this->m_lvl,  out)
        );
}


/**
 * Read an object of the current type from the specified pointer and use its
 * values to override the current object. Note: this implementation does NOT
 * clear existing content from a store. Read item objects will be appended.
 *
 * @param  in Pointer to an input file; positioned at the desired location.
 * @return    True if the object successfully read; otherwise false.
 */
bool Player::read_object(FILE * in)
{
    return (
        read(this->m_name,	     in) &&
		read(this->m_password,   in) &&
        read(this->m_race,       in) &&
        read(this->m_class,      in) &&
        read(this->m_hp,   in) &&
        read(this->m_ob,      in) &&
        read(this->m_db,  in) &&
        read(this->m_mm,    in) &&
        read(this->m_perception, in) &&
		read(this->m_detectTrap,      in) &&
        read(this->m_useItem,  in) &&
        read(this->m_strength,    in) &&
        read(this->m_agility, in) &&
		read(this->m_intelligence, in) &&
		read(this->m_intuition,      in) &&
        read(this->m_presence,  in) &&
        read(this->m_skillPoints,    in) &&
        read(this->m_exp, in) &&
        read(this->m_lvl,  in)
        );
}