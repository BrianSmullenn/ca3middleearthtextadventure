#ifndef WEAPON_H
#define WEAPON_H
//Another extension of Item class. Will be used to add towards player_ob.
#include "Item.h"
#include "Room.h"

class Weapon : public Item
{
private:
	std::string m_use;
	int m_damage;
public:
	Weapon::Weapon();
	Weapon(const std::string &m_name, const std::string &m_desc, int damage);
	void useItem();
	int getDamage();
	void print();

	void save(std::string& filename);
	void load(std::string& filename);
	void read_object(FILE * in);
	void write_object(FILE * out);
};
#endif